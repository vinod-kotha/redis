package redis

import "github.com/gomodule/redigo/redis"

func Set(params ...interface{}) (string, error) {
	return redis.String(Run("SET", params...))
}
