package redis

import (
	"fmt"
	"newtb/TBConstants"
	"time"
)

func main() {
	TBConstants.Init("env.json")
	config := Config{
		Server:      TBConstants.CUR_ENV.REDIS_SERVER,
		Password:    TBConstants.CUR_ENV.REDIS_PASSWORD,
		MaxIdle:     3,
		IdleTimeout: 240 * time.Second,
	}
	InitPool(config)

	result, err := Set("key2", "value2", "EX", 100, "NX")
	if err != nil {
		panic(err)
	}
	fmt.Println(result)
}
